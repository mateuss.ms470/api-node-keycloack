// const baseUrl = 'http://127.0.0.1:8180';

// export default {
//   baseUrl: baseUrl,
//   token: {
//     username: 'alice',
//     password: 'alice',
//     grant_type: 'password',
//     client_id: 'test-cli',
//     realmName: 'quickstart'
//   },
//   adminClient: {
//     baseUrl: baseUrl,
//     realmName: 'master',
//     username: 'admin',
//     password: 'admin',
//     grantType: 'password',
//     clientId: 'admin-cli'
//   }
// };

// config.ts

export const baseUrl = 'http://127.0.0.1:8180';

const config = {
  baseUrl: baseUrl,
  token: {
    username: 'alice',
    password: 'alice',
    grantType: 'password', // Se você estiver usando JSON puro, mantenha isso como string. Se TypeScript, talvez tenha que ajustar para a enumeração.
    clientId: 'test-cli',
    realmName: 'quickstart'
  },
  adminClient: {
    baseUrl: baseUrl,
    realmName: 'master',
    username: 'admin',
    password: 'admin',
    grantType: 'password', // Da mesma forma, ajuste conforme necessário.
    clientId: 'admin-cli'
  }
};

export default config;

