import express, { Request, Response } from 'express';
import Keycloak from 'keycloak-connect';

const app = express();
const port = 3000;

// Middleware configuration loaded from keycloak.json file.
const keycloak = new Keycloak({});

app.use(keycloak.middleware());

app.get('/public', (req: Request, res: Response) => {
  res.json({ message: 'Essa rota é publica' });
});

app.get('/protected', keycloak.protect('realm:user'), (req: Request, res: Response) => {
  res.json({ message: 'Essa rota é protegida' });
});

app.get('/admin', keycloak.protect('realm:admin'), (req: Request, res: Response) => {
  res.json({ message: 'Essa rota é para administradores' });
});

app.use('*', (req: Request, res: Response) => {
  res.send('Not found!');
});

app.listen(port, () => {
  console.log(`Listening on port ${port}.`);
});
