import KcAdminClient from '@keycloak/keycloak-admin-client';
import config from '../config/config';
const adminClient = new KcAdminClient(config.adminClient);
await adminClient.auth({
    ...config.adminClient,
    grantType: config.adminClient.grantType
});
export default adminClient;
